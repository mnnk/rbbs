from django import forms
from django.forms import ModelForm
from .models import Post
from django.utils.translation import ugettext_lazy as _

# Form for entering a post
# Now defunct
class PostForm(forms.Form):
	title = forms.CharField(label="Post title", max_length=200)
	poster = forms.CharField(label="Name", max_length=30)

# Form mapping to creating a new post
class NewPostForm(ModelForm):
	class Meta:
		model = Post
		fields = ['poster', 'title', 'content', 'upload']
		labels = {
				'poster': _('Name'),
				'title': _('Title'),
				'content': _(''),
				'upload': _('Upload file: ')
				}
