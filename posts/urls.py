from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^uploads/uploads/(?P<file>[\w.+])$', views.uploads, name='uploads'),
	url(r'^(?P<postid>[0-9]+)/$', views.posts, name='post'),
]
