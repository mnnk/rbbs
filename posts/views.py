# System includes
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

# Internal includes 
from .models import Post
from .forms import PostForm, NewPostForm


# Views

# /
# Renders all top level posts (ie posts made without a parent)
def index(request):
	allposts = Post.objects.filter(parent__isnull=True).order_by("-date")
	context = {	"allposts": allposts,
				}
	return render(request, "posts/allposts.html", context)

# /p/postid
# Renders a single post and its children
def posts(request, postid):
	try:
		rpost = Post.objects.get(pk=postid)
		if rpost.bump is True:
			children = Post.objects.filter(parent=(Post.objects.get(pk=postid))).order_by("-date")
		else:
			children = Post.objects.filter(parent=(Post.objects.get(pk=postid))).order_by("pk")
		# Form for adding a new post rendering
		newPostForm = NewPostForm()
		if request.method == 'POST':
			newPostForm = NewPostForm(request.POST, request.FILES)
			if newPostForm.is_valid():
				# Create new post, add to table
				toPost = newPostForm.save(commit=False)
				toPost.parent = Post.objects.get(pk=postid)
				toPost.upload = request.FILES.getlist('upload') # Add uploaded file
				# Saving
				toPost.save()
				newPostForm.save_m2m()
				newPostForm = NewPostForm()

				# Modify the parent post
				# Increment reply counter, update time
				rpost.date = timezone.now()
				rpost.replies = rpost.replies+1
				rpost.save()

				# Make sure it doesn't screw up when you refresh
				return HttpResponseRedirect('')
		context = { "rpost": rpost,
					"children": children,
					"form": newPostForm,
					}
		return render(request, "posts/post.html", context)
	except ObjectDoesNotExist:
		raise Http404("404! Post does not exist.")

def uploads(request):
	return HttpResponse("Uploads go here")

