from django.db import models
from django.utils import timezone 

# Create your models here.

class Post(models.Model):
	# Should the post's replies be "bumped" or displayed in chronological order
	# False = Chronological order
	# True = Most recently replied to first
	bump = models.BooleanField(default=False)
	replies = models.IntegerField(default=0)
	title = models.CharField(max_length=200, blank=True)
	poster = models.CharField(max_length=30, default="Anonymous", blank=True)
	date = models.DateTimeField("Date posted")
	content = models.TextField(null = True, blank = True)
	parent = models.ForeignKey(	'self', 					# Creates recursive relationship
								null = True, blank = True,  # Allows creating null parents
								on_delete=models.CASCADE, 	# Deletes all children if parent deleted
								)
	upload = models.FileField(	upload_to = "uploads/"
								, null = True
								, blank = True
								)
	def __str__(self):
		''' For display in database admin '''
		return "post #" + str(self.pk) + " \"" + self.title + "\""
	def save(self, *args, **kwargs):
		''' Do all that timestamp stuff for post editing / adding '''
		if not self.id:
			self.date = timezone.now()
		return super(Post, self).save(*args, **kwargs)
