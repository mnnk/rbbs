# Really simple recursive bbs system

You create a post, the post can be replied to. That reply can be replied to. And so can the reply to that. Etc, etc, forever.

It's turtles all the way down!

**How to set this project up**
- Clone the repo and cd into its directory: `git clone https://gitlab.com/mnnk/rbbs.git && cd rbbs`
- Create a virtual environment: `virtualenv .`
- Source the environment: `. bin/activate`
- Install django: `pip install django`
- Run the test server: `python3 manage.py runserver`


